import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MedComponent } from './med/med.component';
import { StepperComponent } from './stepper/stepper.component';
import { InsuComponent } from './insu/insu.component';

const routes: Routes = [
  {path:"", component:StepperComponent},
  {path:"Insu", component:InsuComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
