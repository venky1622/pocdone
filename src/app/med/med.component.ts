import { Component, ElementRef, ViewChild, OnInit, Output, EventEmitter } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatAutocompleteSelectedEvent, MatChipInputEvent, MatAutocomplete } from '@angular/material';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as XLSX from 'ts-xlsx';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-med',
  templateUrl: './med.component.html',
  styleUrls: ['./med.component.css']
})
export class MedComponent implements OnInit {

  @Output() navigate: EventEmitter<any> = new EventEmitter<any>();

  arrayBuffer:any;
  file:File;
  obj: any = {};
  selectable = true;
  removable = true;
  crossMark: boolean = false;
  pocOptData = new FormControl();
  delItem: any ;
  filteredOptData: Observable<string[]>;
  selOptions: string[] = [];
  itemInvalid:boolean=false;
  allAutoOptions: string[] = ['Ibuprofen', 'stannous fluoride', 'Acetaminophen', 'Aluminum Zirconium', 'SODIUM FLUORIDE', 'Methocarbamol', 'ETHYL ALCOHOL'];


  @ViewChild('optTextArea') optTextArea: ElementRef<HTMLInputElement>;
  @ViewChild('auto') matAutocomplete: MatAutocomplete;
  itemCall: any;



  constructor(public http: HttpClient) {

    const strs = ['valval', 'bal', 'gal', 'dalval'];
    const result = strs.filter(s => s.includes('bal00'));
    this.getData()
    // C:\Users\venky\OneDrive\Desktop\Golden Sun Technology\New folder\pocdone\src\assets\excel\Candidates.xlsx
  }

  getData() {
    // debugger;
    this.filteredOptData = this.pocOptData.valueChanges.pipe(
      map((opt: string) => opt ? this._filter(opt) : this.allAutoOptions.slice()));
  }

  ngOnInit() {
// const fileName='../../assets/excel/Candidates.xlsx'
this.http.get("../../assets/excel/Candidates.xlsx").subscribe(data=>{
  console.log(data);
})
  }

  add(event: MatChipInputEvent): void {
    if (!this.matAutocomplete.isOpen) {
      const input = event.input;
      const value = event.value;
      if ((value || '').trim()) {
        this.selOptions.push(value.trim());
      }
      if (input) {
        input.value = '';
      }
      this.pocOptData.setValue(null);
    }
  }

  rmItem(i) {
    this.delItem = i
  }
  remove(opt: string): void {
    this.delItem=-1
    const index = this.selOptions.indexOf(opt);
    if (index >= 0) {
      this.selOptions.splice(index, 1);
    }
    this.getData()
  }

  selected(event: MatAutocompleteSelectedEvent): void {
    if (this.selOptions.length < 20) {
      var index = this.selOptions.findIndex(x => x === event.option.value);
      if (index === -1) {
        this.selOptions.push(event.option.viewValue);
        this.getData()
        this.obj.items = this.selOptions;
      }
      this.optTextArea.nativeElement.value = '';
      this.pocOptData.setValue(null);

    }
    else {
      this.getData()
      this.optTextArea.nativeElement.value = '';
      this.pocOptData.setValue(null);
    }

  }

  onKeychange(event): void {
      this._filter(event.target.value);
      const textarea = event.target;
      const value = textarea.value;
      this.pocOptData.setValue(value);
      console.log(event)
    
  }

  private _filter(value: string): string[] {
    if(value.length>=3){
      if (value.trim().length) {
        const filterValue = value.toLowerCase();
        return this.allAutoOptions.filter(opt => opt.toLowerCase().indexOf(filterValue) === 0);
      }
      else {
        this.getData()
      }
    }
    else{
      this.getData()
        }
  

  }

  incomingfile(event) 
  {
    console.log(event)
  this.file= event.target.files[0]; 
  }

  Upload() {
    let fileReader = new FileReader();
      fileReader.onload = (e) => {
          this.arrayBuffer = fileReader.result;
          var data = new Uint8Array(this.arrayBuffer);
          var arr = new Array();
          for(var i = 0; i != data.length; ++i) arr[i] = String.fromCharCode(data[i]);
          var bstr = arr.join("");
          var workbook = XLSX.read(bstr, {type:"binary"});
          var first_sheet_name = workbook.SheetNames[0];
          var worksheet = workbook.Sheets[first_sheet_name];
          console.log(XLSX.utils.sheet_to_json(worksheet,{raw:true}));
      }
      fileReader.readAsArrayBuffer(this.file);
}

  fnNumbers(event) {
    // return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
    if (event.charCode < 48 || event.charCode > 57) {
      return false;
    }
  }

  fnAlphabets(event) {
    this.itemInvalid=false;
    if (event.charCode != 32 && event.charCode <= 46 || (event.charCode > 46 && event.charCode < 65) || (event.charCode > 90 && event.charCode < 97) || event.charCode > 122) {
      // Restrict Numbers and dot (.)
      
      return false;
    }
    
  }
  nextStep() {
    
         
    console.log(this.itemInvalid)
    if (this.obj.name !== undefined && this.obj.number !== undefined && this.obj.items !== undefined && this.obj.name !== "" && this.obj.number !== "" && this.obj.items.length > 0) {

      if (this.obj.number >= 10) {
        this.navigate.emit(this.obj)
      }

    }
    else{
      this.itemInvalid=true;
    }

  }
}
