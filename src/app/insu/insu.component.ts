import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { PlatformLocation } from '@angular/common';
import { platformBrowser } from '@angular/platform-browser';

@Component({
  selector: 'app-insu',
  templateUrl: './insu.component.html',
  styleUrls: ['./insu.component.css']
})
export class InsuComponent implements OnInit {

  @ViewChild('prescriptionCard') prescriptionCard: ElementRef<HTMLInputElement>;
  
  displayBenifitcard:boolean=true;
  displayMedicalCard:boolean=true;
  displayForm:boolean=false;
  obj:any;
  constructor() { 
  }

  ngOnInit() {
  
  }
  
  nobtn(){
this.displayForm=true;
  }

  yesbtn(){
    this.displayForm=false;
  }


  benefitCard(event){
    console.log(event.target.checked)
    // console.log(this.obj)
    // if (this.prescriptionCard.checked == true){
    //   text.style.display = "block";
    // } 
if(event.target.checked==true){
this.displayBenifitcard=false;
}
else{
  this.displayBenifitcard=true;
}
  }

  medicalCard(event){
    if(event.target.checked==true){
    this.displayMedicalCard=false;
    }
    else{
      this.displayMedicalCard=true;
    }

  }
}
