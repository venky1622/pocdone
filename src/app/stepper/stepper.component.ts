import { Component, OnInit, ViewChild } from '@angular/core';
import { MatStepper } from '@angular/material';
// import { MatStepper } from '@angular/material/stepper';
import { PlatformLocation } from '@angular/common' 

@Component({
  selector: 'app-stepper',
  templateUrl: './stepper.component.html',
  styleUrls: ['./stepper.component.css']
})
export class StepperComponent implements OnInit {
  @ViewChild('stepper') stepper: MatStepper;
  constructor(location: PlatformLocation) { }

  ngOnInit() {

    this.stepper.selectedIndex = 2; 
  }

  ngOnChanges(){
    console.log("there")
    window.onbeforeunload = function() {  
      //Do your stuff here for open google.com  
      location.href="www.google.com" 
  } 

  location.href="www.google.com" 
  }


  itemNavigate(items){
    console.log("hello there", items)
    this.stepper.selectedIndex = 2; 
  }
}
